import { Component, OnInit } from '@angular/core';
declare var google: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

    map: any;
    constructor() { }

    ngOnInit() {
        this.initMap()
    }

    initMap() {
        var that = this;
        this.map = new google.maps.Map(document.getElementById('map'), {
            center: { 
                lat: 19.4233466, 
                lng: -99.2588329 
            },
            zoom: 8,
        });
    }

}
